package pl.adgroup.drivinglicence.component;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.adgroup.drivinglicence.dto.RequestLicence;
import pl.adgroup.drivinglicence.service.LicenceService;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private LicenceService licenceService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        createInitialLicences();
    }

    private void createInitialLicences() {
        RequestLicence licence1 = createLicence("Adam","Nowak","1980-11-01","Warszawa",
                "2000-03-04","2020-12-12",3204,
                80110105661L,"01234/00/3204","A,B,C",
                "00-100","Warszawa","Długa","1A");
        licenceService.addLicenceWithAddress(licence1);

        RequestLicence licence2 = createLicence("Bolesław","Kowalski","1956-12-03","Kraków",
                "1975-03-04","2020-12-31",5907,
                56120356611L,"05678/00/5907","B1,B,D",
                "00-500","Kraków","Płaska","2B");
        licenceService.addLicenceWithAddress(licence2);

        RequestLicence licence3 = createLicence("Adam","Mickiewicz","1950-03-03","Płock",
                "1981-03-04","2020-12-12",3333,
                80210105661L,"09101/00/3333","A,B,C",
                "00-600","Płock","Szeroka","10D/78");
        licenceService.addLicenceWithAddress(licence3);

        RequestLicence licence4 = createLicence("Henryk","Sienkiewicz","1970-10-01","Gdańsk",
                "1982-03-04","2020-12-12",4444,
                70110103331L,"12131/00/4444","A,B",
                "00-700","Gdańsk","Piwna","3F/12");
        licenceService.addLicenceWithAddress(licence4);

        RequestLicence licence5 = createLicence("Maria","Konopnicka","1960-11-01","Bydgoszcz",
                "1983-03-04","2020-12-12",5555,
                60110105661L,"15161/00/5555","A,B",
                "00-800","Bydgoszcz","Krzywa","23A");
        licenceService.addLicenceWithAddress(licence5);

        RequestLicence licence6 = createLicence("Bruno","Szulc","1950-11-01","Toruń",
                "1984-03-04","2020-12-12",6666,
                50110105661L,"17181/00/6666","A",
                "00-900","Toruń","Wspólna","20");
        licenceService.addLicenceWithAddress(licence6);

        RequestLicence licence7 = createLicence("Stanisław","Lem","1940-11-01","Olsztyn",
                "1985-03-04","2020-12-12",7777,
                40110105661L,"19202/00/7777","A,B,C,D",
                "01-000","Olsztyn","Ciepła","45A/12");
        licenceService.addLicenceWithAddress(licence7);

        RequestLicence licence8 = createLicence("Maciej","Dukaj","1967-11-01","Łomża",
                "1985-03-04","2020-12-12",8888,
                67110105661L,"21222/00/8888","A,B,D",
                "02-000","Łomża","Zimna","4B");
        licenceService.addLicenceWithAddress(licence8);

        RequestLicence licence9 = createLicence("Stanisław","Moniuszko","1959-11-01","Gdynia",
                "1985-03-04","2020-12-12",9999,
                59110105661L,"23242/00/9999","A,B,C",
                "03-000","Gdynia","Wąska","28");
        licenceService.addLicenceWithAddress(licence9);

        RequestLicence licence10 = createLicence("Fryderyk","Chopin","1983-11-01","Augustów",
                "1985-03-04","2020-12-12",1111,
                83110105661L,"25262/00/1111","A,B1,B",
                "04-000","Suwałki","Monte Cassino","1");
        licenceService.addLicenceWithAddress(licence10);

        RequestLicence licence11 = createLicence("Ignacy","Krasicki","1972-11-01","Lublin",
                "1985-03-04","2020-12-12",2222,
                72110105661L,"27282/00/2222","A,B,C",
                "05-000","Rzeszów","Toruńska","10C/34");
        licenceService.addLicenceWithAddress(licence11);

        RequestLicence licence12 = createLicence("Bolesław","Chrobry","1993-11-01","Kraków",
                "1985-03-04","2020-12-12",7777,
                93110105661L,"29303/00/7777","A,B",
                "06-000","Olsztyn","Warszawska","23");
        licenceService.addLicenceWithAddress(licence12);

        RequestLicence licence13 = createLicence("Bolesław","Prus","1991-11-01","Gdańsk",
                "1985-03-04","2020-12-12",7777,
                91110105661L,"31323/00/7777","A",
                "07-000","Gdańsk","Sternicza","1A/14");
        licenceService.addLicenceWithAddress(licence13);

        RequestLicence licence14 = createLicence("Sławomir","Mrożek","1999-11-01","Malbork",
                "1985-03-04","2020-12-12",7777,
                99110105661L,"24252/00/7777","A",
                "08-000","Malbork","Myśliwska","1A/12");
        licenceService.addLicenceWithAddress(licence14);

        RequestLicence licence15 = createLicence("Wisława","Szymborska","37-11-01","Sopot",
                "1985-03-04","2020-12-12",7777,
                37110105661L,"26272/00/7777","B,C",
                "09-000","Sopot","Monte Cassino","103");
        licenceService.addLicenceWithAddress(licence15);


        RequestLicence licence16 = createLicence("Julian","Tuwim","1978-11-01","Poznań",
                "1985-03-04","2020-12-12",7777,
                78110105661L,"28292/00/7777","B,C",
                "10-000","Olsztyn","Poznańska","2B/3");
        licenceService.addLicenceWithAddress(licence16);

        RequestLicence licence17 = createLicence("Stanisław","Lem","1997-11-01","Gdańsk",
                "1985-03-04","2020-12-12",7777,
                97110105661L,"30313/00/7777","B",
                "11-000","Warszawa","Targ Węglowy","10");
        licenceService.addLicenceWithAddress(licence17);

        RequestLicence licence18 = createLicence("Witold","Gombrowicz","1945-11-14","Opole",
                "1985-03-04","2020-12-12",1234,
                45110105661L,"32343/00/1234","B",
                "12-000","Wrocław","Opolska","10");
        licenceService.addLicenceWithAddress(licence18);

        RequestLicence licence19 = createLicence("Stanisław","Staszic","1965-11-23","Sopot",
                "1985-03-04","2020-12-12",5678,
                65110105661L,"35363/00/5678","B",
                "13-000","Wrocław","Haffnera","45");
        licenceService.addLicenceWithAddress(licence19);

        RequestLicence licence20 = createLicence("Stefan","Żeromski","1961-11-23","Wrocław",
                "1985-03-04","2020-12-12",9876,
                61110105661L,"37383/00/9876","B",
                "14-000","Wrocław","Szpitalna","99/13");
        licenceService.addLicenceWithAddress(licence20);
    }


    private RequestLicence createLicence (String firstName, String lastName, String birthDate,
             String birthPlace, String creationDate, String expiryDate, int creationInstitution,
             long pesel, String licenceNumber, String categories, String postCode,
             String city, String street, String houseNumber)
    {
        RequestLicence licence = new RequestLicence();
        licence.setFirstName(firstName);
        licence.setLastName(lastName);
        licence.setBirthDate(birthDate);
        licence.setBirthPlace(birthPlace);
        licence.setCreationDate(creationDate);
        licence.setExpiryDate(expiryDate);
        licence.setCreationInstitution(creationInstitution);
        licence.setPesel(pesel);
        licence.setLicenceNumber(licenceNumber);
        licence.setCategories(categories);
        licence.setPostCode(postCode);
        licence.setCity(city);
        licence.setStreet(street);
        licence.setHouseNumber(houseNumber);

        return licence;
    }


}
