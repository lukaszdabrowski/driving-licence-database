package pl.adgroup.drivinglicence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.adgroup.drivinglicence.entity.Licence;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface LicenceRepository extends JpaRepository<Licence,Long> {

    Optional<Licence> findLicenceByPesel (Long pesel);

    Optional<Licence> findById (Long id);

    List<Licence> findLicenceByFirstName (String firstName);

   List<Licence> findLicenceByLastName (String lastName);

   Optional<Licence> findLicenceByLicenceNumber (String licenceNumber);

   List<Licence> findLicenceByBirthDate (Date birthDate);

   List<Licence> findLicenceByBirthPlace (String birthPlace);

   List<Licence> findLicenceByExpiryDate (Date expiryDate);

   List<Licence> findLicenceByCreationDate (Date creationDate);

   List<Licence> findLicenceByCreationInstitution (int creationInstitution);

//    @Query(nativeQuery = true, value = "select * from LICENCE where id in ( select id from LICENCE, unnest(categories) as categories(c) where c in(:categoriesArray))")
//    List<Licence> findByCategory(@Param("categoriesArray") String[] categoriesArray);

}
