package pl.adgroup.drivinglicence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.adgroup.drivinglicence.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
