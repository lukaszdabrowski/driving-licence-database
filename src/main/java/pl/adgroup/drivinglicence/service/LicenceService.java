package pl.adgroup.drivinglicence.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.adgroup.drivinglicence.dto.RequestLicence;
import pl.adgroup.drivinglicence.entity.Address;
import pl.adgroup.drivinglicence.entity.Licence;
import pl.adgroup.drivinglicence.repository.AddressRepository;
import pl.adgroup.drivinglicence.repository.LicenceRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LicenceService {

    @Autowired
    LicenceRepository licenceRepository;

    @Autowired
    AddressRepository addressRepository;

    public Optional<Licence> addLicenceWithAddress(RequestLicence requestLicence) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Optional<Licence> licencePeselOptional = licenceRepository
                .findLicenceByPesel(requestLicence.getPesel());
        Optional<Licence> licenceNumberOptional = licenceRepository
                .findLicenceByLicenceNumber(requestLicence.getLicenceNumber());

        if (!licencePeselOptional.isPresent() && !licenceNumberOptional.isPresent()) {

            Licence licence = new Licence();

            licence.setFirstName(requestLicence.getFirstName());
            licence.setLastName(requestLicence.getLastName());
            licence.setPesel(requestLicence.getPesel());
            licence.setLicenceNumber(requestLicence.getLicenceNumber());
            licence.setCategories(requestLicence.getCategories().split(","));
            licence.setCreationInstitution(requestLicence.getCreationInstitution());
            licence.setBirthPlace(requestLicence.getBirthPlace());

            String birthDateString = requestLicence.getBirthDate();

            try {
                Date birthDateFromDto = dateFormat.parse(birthDateString);
                licence.setBirthDate(birthDateFromDto);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            String creationDateString = requestLicence.getCreationDate();

            try {
                Date creationDateFromDto = dateFormat.parse(creationDateString);
                licence.setCreationDate(creationDateFromDto);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            String expiryDateString = requestLicence.getExpiryDate();

            try {
                Date expiryDateFromDto = dateFormat.parse(expiryDateString);
                licence.setExpiryDate(expiryDateFromDto);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            Address address = new Address();

            address.setCity(requestLicence.getCity());
            address.setStreet(requestLicence.getStreet());
            address.setHouseNumber(requestLicence.getHouseNumber());
            address.setPostCode(requestLicence.getPostCode());

            licence.setAddress(address);

            addressRepository.save(address);

            return Optional.of(licenceRepository.save(licence));
        }
        return Optional.empty();
    }

    public List<RequestLicence> getAllLicences() {
        List<Licence> licences = licenceRepository.findAll();
        List<RequestLicence> licenceDtos = mapLicences(licences);
        return licenceDtos;
    }

    public List<RequestLicence> searchLicences(String query) {
        List<Licence> licenceDtos = new ArrayList<>();

        try {
            licenceDtos.add(licenceRepository.findLicenceByPesel(Long.valueOf(query)).get());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            List<Licence> licencesFirstName = licenceRepository.findLicenceByFirstName(query);
            for (Licence l : licencesFirstName) {
                licenceDtos.add(l);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            List<Licence> licencesLastName = licenceRepository.findLicenceByLastName(query);
            for (Licence l : licencesLastName) {
                licenceDtos.add(l);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String[] nameArray = query.split(" ");
            List<Licence> licencesFirstName = licenceRepository.findLicenceByFirstName(nameArray[0]);
            for (Licence l : licencesFirstName) {
                if (l.getLastName() == nameArray[1])
                licenceDtos.add(l);
            }
            List<Licence> licencesLastName = licenceRepository.findLicenceByFirstName(nameArray[1]);
            for (Licence l : licencesLastName) {
                if (l.getFirstName() == nameArray[0])
                licenceDtos.add(l);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            List<Licence> licencesInstitution = licenceRepository.findLicenceByCreationInstitution(Integer.valueOf(query));
            for (Licence l : licencesInstitution) {
                licenceDtos.add(l);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            licenceDtos.add(licenceRepository.findLicenceByLicenceNumber(query).get());
        }

        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            DateFormat formatter = new SimpleDateFormat("dd.MM.yy");
            Date birthDate = formatter.parse(query);
            List<Licence> licencesBirthDate = licenceRepository.findLicenceByBirthDate(birthDate);
            for (Licence l : licencesBirthDate) {
                licenceDtos.add(l);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<RequestLicence> licences;

        if (!licenceDtos.isEmpty()) {
            licences = mapLicences(licenceDtos);
            return licences;
        }
        else {
            licences = new ArrayList<>();
            return licences;
        }
    }

    public List<RequestLicence> mapLicences (List<Licence> licences) {
        List<RequestLicence> licencesDtos = licences
                .stream()
                .map(temp -> {
                    RequestLicence requestLicence = new RequestLicence();
                    requestLicence.setId(temp.getId());
                    requestLicence.setFirstName(temp.getFirstName());
                    requestLicence.setLastName(temp.getLastName());
                    requestLicence.setCategories(Arrays.toString(temp.getCategories())
                            .replace("[","")
                            .replace("]",""));

                    DateFormat formatter = new SimpleDateFormat("dd.MM.yy");

                    String formattedCreationDate = formatter.format(temp.getCreationDate());
                    requestLicence.setCreationDate(formattedCreationDate);

                    String formattedExpiryDate = formatter.format(temp.getExpiryDate());
                    requestLicence.setExpiryDate(formattedExpiryDate);

                    String formattedBirthDate = formatter.format(temp.getBirthDate());
                    requestLicence.setBirthDate(formattedBirthDate);

                    requestLicence.setBirthPlace(temp.getBirthPlace());

                    requestLicence.setCreationInstitution(temp.getCreationInstitution());
                    requestLicence.setLicenceNumber(temp.getLicenceNumber());
                    requestLicence.setPesel(temp.getPesel());
                    requestLicence.setCity(temp.getAddress().getCity());
                    requestLicence.setStreet(temp.getAddress().getStreet());
                    requestLicence.setHouseNumber(temp.getAddress().getHouseNumber());
                    requestLicence.setPostCode(temp.getAddress().getPostCode());
                    return requestLicence;
                }).collect(Collectors.toList());
        return licencesDtos;
    }

    public Optional<RequestLicence> getLicenceById(Long id) {
        Optional<Licence> licenceOptional = licenceRepository.findById(id);
        if (licenceOptional.isPresent()) {
            Licence licence = licenceOptional.get();
            RequestLicence requestLicence = new RequestLicence();
            requestLicence.setId(licence.getId());
            requestLicence.setFirstName(licence.getFirstName());
            requestLicence.setLastName(licence.getLastName());
            requestLicence.setCategories(Arrays.toString(licence.getCategories())
                    .replace("[","")
                    .replace("]",""));

            DateFormat formatter = new SimpleDateFormat("dd.MM.yy");

            String formattedCreationDate = formatter.format(licence.getCreationDate());
            requestLicence.setCreationDate(formattedCreationDate);

            String formattedExpiryDate = formatter.format(licence.getExpiryDate());
            requestLicence.setExpiryDate(formattedExpiryDate);

            String formattedBirthDate = formatter.format(licence.getBirthDate());
            requestLicence.setBirthDate(formattedBirthDate);

            requestLicence.setBirthPlace(licence.getBirthPlace());

            requestLicence.setCreationInstitution(licence.getCreationInstitution());
            requestLicence.setLicenceNumber(licence.getLicenceNumber());
            requestLicence.setPesel(licence.getPesel());
            requestLicence.setCity(licence.getAddress().getCity());
            requestLicence.setStreet(licence.getAddress().getStreet());
            requestLicence.setHouseNumber(licence.getAddress().getHouseNumber());
            requestLicence.setPostCode(licence.getAddress().getPostCode());

            return Optional.of(requestLicence);
        }

        return Optional.empty();
    }

    public void deleteById (Long id) {
        licenceRepository.deleteById(id);
    }

    public void update (Long id, RequestLicence requestLicence) {
        Optional<Licence> licenceOptional = licenceRepository
                .findById(id);

        if (!licenceOptional.isPresent()) {
            Licence licence = licenceOptional.get();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            licence.setFirstName(requestLicence.getFirstName());
            licence.setLastName(requestLicence.getLastName());
            licence.setPesel(requestLicence.getPesel());
            licence.setLicenceNumber(requestLicence.getLicenceNumber());
            licence.setCategories(requestLicence.getCategories().split(","));
            licence.setCreationInstitution(requestLicence.getCreationInstitution());
            licence.setBirthPlace(requestLicence.getBirthPlace());
            String birthDateString = requestLicence.getBirthDate();

            try {
                Date birthDateFromDto = dateFormat.parse(birthDateString);
                licence.setBirthDate(birthDateFromDto);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            String creationDateString = requestLicence.getCreationDate();

            try {
                Date creationDateFromDto = dateFormat.parse(creationDateString);
                licence.setCreationDate(creationDateFromDto);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            String expiryDateString = requestLicence.getExpiryDate();

            try {
                Date expiryDateFromDto = dateFormat.parse(expiryDateString);
                licence.setExpiryDate(expiryDateFromDto);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            Address address = licence.getAddress();
            address.setCity(requestLicence.getCity());
            address.setStreet(requestLicence.getStreet());
            address.setHouseNumber(requestLicence.getHouseNumber());
            address.setPostCode(requestLicence.getPostCode());

            licence.setAddress(address);

            addressRepository.save(address);
        }
    }

}
