package pl.adgroup.drivinglicence.controller;

import org.springframework.web.bind.annotation.*;
import pl.adgroup.drivinglicence.dto.RequestLicence;
import pl.adgroup.drivinglicence.service.LicenceService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RestLicenceController {

    private LicenceService licenceService;

    public RestLicenceController (LicenceService licenceService) {
        this.licenceService = licenceService;
    }

    @GetMapping
    public List<RequestLicence> getAll() {
        return licenceService.getAllLicences();
    }

    @PostMapping
    public void create(@RequestBody RequestLicence requestLicence) {
        licenceService.addLicenceWithAddress(requestLicence);
    }


    @GetMapping("/{id}")
    public RequestLicence getOne(@PathVariable Long id) {
        return licenceService.getLicenceById(id).get();
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody RequestLicence requestLicence) {
        licenceService.update(id,requestLicence);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        licenceService.deleteById(id);
    }
}
