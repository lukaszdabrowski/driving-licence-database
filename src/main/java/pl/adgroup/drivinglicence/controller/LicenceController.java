package pl.adgroup.drivinglicence.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.adgroup.drivinglicence.dto.RequestLicence;
import pl.adgroup.drivinglicence.entity.Licence;
import pl.adgroup.drivinglicence.service.LicenceService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class LicenceController {

    private LicenceService licenceService;

    public LicenceController(LicenceService licenceService) {
        this.licenceService = licenceService;
    }


    @GetMapping
    public String listAllLicences (Model model) {
        List<RequestLicence> licences = licenceService.getAllLicences();
        model.addAttribute("licences",licences);
        return "index";
    }


    @GetMapping("/search")
    public String searchLicences (@RequestParam(required = false) String query, Model model) {
        if (query == null) {
            List<RequestLicence> licences = licenceService.getAllLicences();
            model.addAttribute("licences", licences);
            return "search";
        }
        else {
            List<RequestLicence> licences = licenceService.searchLicences(query);
            model.addAttribute("licences",licences);
            return "search";
        }
    }

    @PostMapping("/search")
    public String sendQuery (@RequestParam String query, Model model, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("query", query);
        return "redirect:/search";
    }

    @GetMapping("/add")
    public String addAndlist(Model model) {
    List<RequestLicence> licences = licenceService.getAllLicences();
        model.addAttribute("licences",licences);
        return "add";
    }

    @PostMapping("/add")
    public String add(@Valid @ModelAttribute("licence") RequestLicence licence, BindingResult error,
                      Model model)
    {
        licenceService.addLicenceWithAddress(licence);
        return "redirect:/add";
    }

    @ModelAttribute("licence")
    public RequestLicence licence() {
        return new RequestLicence();
    }


}
