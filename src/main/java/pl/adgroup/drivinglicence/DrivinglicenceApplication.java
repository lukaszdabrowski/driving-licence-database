package pl.adgroup.drivinglicence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrivinglicenceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DrivinglicenceApplication.class, args);
    }

}

